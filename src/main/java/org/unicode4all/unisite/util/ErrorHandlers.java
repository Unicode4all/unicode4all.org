package org.unicode4all.unisite.util;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.unicode4all.unisite.util.exception.ResourceNotFoundException;

/**
 * Created by combi on 13.02.2016.
 */
public class ErrorHandlers {
    @ExceptionHandler(ConstraintViolationException.class)
    public String notFoundHandler() {
        return "404";
    }
}
