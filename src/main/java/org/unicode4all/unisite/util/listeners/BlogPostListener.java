package org.unicode4all.unisite.util.listeners;

import org.unicode4all.unisite.models.BlogPost;

import javax.persistence.PrePersist;
import java.util.Calendar;

/**
 * Created by combi on 16.02.2016.
 */
public class BlogPostListener {

    @PrePersist
    public void setDate(BlogPost post) {
        post.setDate(Calendar.getInstance().getTime());
    }
}
