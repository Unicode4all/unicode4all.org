package org.unicode4all.unisite.controllers.rest;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.unicode4all.unisite.models.BlogParams;
import org.unicode4all.unisite.models.BlogPost;
import org.unicode4all.unisite.services.BlogService;

import javax.annotation.Resource;
import javax.json.Json;
import javax.json.JsonObject;
import java.security.Principal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by uni on 29.02.16.
 */
@RestController
@RequestMapping("/rest/blog")
public class BlogRestController {
    @Resource(name = "blogPostService")
    BlogService blogService;

    @RequestMapping (value = "/posts.json", headers="Accept=*/*", method = RequestMethod.GET, produces = "application/json")
    public List<BlogPost> getPosts() {
        List<BlogPost> posts = blogService.getAll();
        return posts;
    }

    @RequestMapping(value = "/{name}.json", headers="Accept=*/*", method = RequestMethod.GET, produces = "application/json")
    public BlogPost getPost(@PathVariable("name") String name) {
        BlogPost post = blogService.getByName(name);
        return post;
    }


    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/post", method = RequestMethod.PUT)
    public Map<String,Object> createOrUpdatePost(@ModelAttribute("post") BlogPost post) {
        Map<String, Object> model = new HashMap<String, Object>();
        BlogParams params = new BlogParams();
        BlogPost epost = blogService.getByName(post.getName());
        if (epost == null) {
            post.setDate(Calendar.getInstance().getTime());
            blogService.add(post, params);
            model.put("Status","Added");
        }
        else {
            epost.setTitle(post.getTitle());
            epost.setImagePath(post.getImagePath());
            epost.setText(post.getText());
            blogService.edit(epost);
            model.put("Status","Updated");
        }
        return model;
    }
}
