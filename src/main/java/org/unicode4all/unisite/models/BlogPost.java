package org.unicode4all.unisite.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.unicode4all.unisite.util.listeners.BlogPostListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@EntityListeners(BlogPostListener.class)
@Table(name = "unisite.posts")
public class BlogPost implements Serializable {
    private static final long serialVersionUID = 3447002527667667353L;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "post", cascade = CascadeType.ALL)
    private BlogParams blogParams;

    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue
    private Integer id;
    @Column(name = "title")
    private String title;
    @Column(name = "date")
    private Date date;
    @Column(name = "text", length = 100000)
    private String text;
    @Column(name = "image_path")
    private String imagePath;
    @Column(name = "name", unique = true)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public BlogParams getBlogParams() {
        return this.blogParams;
    }

    public void setBlogParams(BlogParams blogParams) {
        this.blogParams = blogParams;
    }


}
