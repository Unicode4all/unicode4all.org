/*
 *    Copyright 2016 Unicode4all
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.unicode4all.unisite.services;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.unicode4all.unisite.models.User;
import org.unicode4all.unisite.models.UserRole;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Unicode4all on 17.03.2016.
 */

@Service("userService")
@Transactional
public class UserService {

    @Resource
    SessionFactory sessionFactory;

    public List getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User u");
        return query.list();
    }

    public User getUserByName(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where username=:username");
        query.setParameter("username", username);
        return (User) query.uniqueResult();
    }

    public User getUserById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        return (User) session.get(User.class, id);
    }

    public void createUser(User user, UserRole roles) {
        Session session = sessionFactory.getCurrentSession();
        user.getUserRole().add(roles);
        session.save(user);
    }

    public void updateUser(User user) {
        //TODO: update user
    }

    public void updateUser(User user, UserRole role) {
        //TODO: update user with their role
    }

    public void deleteUser(User user) {
        //TODO: delete user
    }
}
