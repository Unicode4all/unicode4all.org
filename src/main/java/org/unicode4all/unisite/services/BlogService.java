package org.unicode4all.unisite.services;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import org.unicode4all.unisite.models.BlogParams;
import org.unicode4all.unisite.models.BlogPost;

import java.util.List;

@Service("blogPostService")
@Transactional
public class BlogService {

    @Resource(name="sessionFactory")
    private SessionFactory sessionFactory;


    /**
     * Retrieves all blog posts
     * @return a list of all posts
     */
    public List getAll() {

        Session session = sessionFactory.getCurrentSession();

        Query query =  session.createQuery("from BlogPost p");

        return query.list();
    }

    /**
     * Retrieves a single post
     * @param id a post id
     * @return a single post object
     */
    public BlogPost get(int id) {
        Session session = sessionFactory.getCurrentSession();

        return (BlogPost) session.get(BlogPost.class, id);
    }

    public BlogPost getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from BlogPost p where name=:name");
        query.setParameter("name", name);

        return (BlogPost) query.uniqueResult();
    }

    /**
     * Stores a post in database
     * @param post - a post object
     */

    public void add(BlogPost post, BlogParams params) {
        Session session = sessionFactory.getCurrentSession();

        post.setBlogParams(params);
        session.save(post);
    }

    public void delete(int id) {

        Session session = sessionFactory.getCurrentSession();

        BlogPost post = (BlogPost) session.get(BlogPost.class, id);
        try {
            session.delete(post);
        }
        catch(HibernateException e) {
        }
    }

    public void edit(BlogPost post) {

        Session session = sessionFactory.getCurrentSession();

        BlogPost existingPost = (BlogPost) session.get(BlogPost.class, post.getId());

        existingPost.setDate(post.getDate());
        existingPost.setText(post.getText());
        existingPost.setTitle(post.getTitle());

        session.save(existingPost);
    }
}
