unisite.controller('Index', function ($scope, $mdSidenav, $http, $mdDialog, $rootScope, $mdToast) {
    $http.get('user').success(function (data) {
        if (data.authenticated) {
            $rootScope.authenticated = true;
            $rootScope.username = data.username;
        } else {
            $rootScope.authenticated = false;
        }
    }).error(function () {
        $rootScope.authenticated = false;
    });

    $scope.theme = 'lime';
    $scope.openLeftMenu = function () {
        $mdSidenav('left').toggle();
        $scope.imagePath = 'img/washedout.png';
    };
    $scope.showLoginDialog = function ($event) {
        var parent = angular.element(document.body);
    $mdDialog.show({
        parent: parent,
        targetEvent: $event,
        templateUrl: "resources/pages/login.html",
        controller: 'loginController',
        controllerAs: 'controller',
        clickOutsideToClose:true
    });
    }

    this.logout = function() {
        $http.post('logout', {}).finally(function() {
            $rootScope.authenticated = false;
            $mdToast.showSimple('Succesfully logged out!');
        })
    }

    $scope.pages = [
        {
            name: 'Home',
            url: '/',
            icon: 'home'
        },
        {
            name: 'About',
            url: '/about'
        },
        {
            name: 'Login',
            url: '/login'
        },
        {
            name: 'Register',
            url: '/register'
        }
    ];

    $scope.themes = ['lime', 'indigo'];

    var originatorEv;
    this.openMenu = function ($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
    };


});
