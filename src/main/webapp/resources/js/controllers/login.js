unisite.controller('loginController', function ($scope, $mdDialog, $http, $rootScope,$mdToast) {
    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    var self = this

    var authenticate = function (credentials, callback) {

        var headers = credentials ? {
            authorization: "Basic "
            + btoa(credentials.username + ":" + credentials.password)
        } : {};

        $http.get('user', {headers: headers}).success(function (data) {
            if (data.name) {
                $rootScope.authenticated = true;
            } else {
                $rootScope.authenticated = false;
            }
            callback && callback();
        }).error(function () {
            $rootScope.authenticated = false;
            callback && callback();
        });

    }

    authenticate();
    self.credentials = {};
    self.login = function () {
        authenticate(self.credentials, function () {
            if ($rootScope.authenticated) {
                $mdDialog.cancel();
                $scope.error = false;
                $mdToast.showSimple('Succesfully authenticated!');
            } else {
                $scope.error = true;
            }
        });
    };
});