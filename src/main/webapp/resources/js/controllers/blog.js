unisite.controller('blogController', function($scope, $mdSidenav, $http, $mdToast, $rootScope) {
    $scope.currentPage = 'Blog Posts';
    $rootScope.loading = true;
    $http.get('/rest/blog/posts.json').success(function(response){
        $rootScope.loading = false;
        $scope.posts = response;
    }).error(function(response){
        if(response.status == 404){
            $rootScope.loading = false;
            $mdToast.showSimple('Posts not found');
        }
    });
});