var unisite = angular.module('unisite', ['ngMaterial', 'ngMdIcons', 'ngRoute', 'ngMessages'])
    .config(function ($mdThemingProvider) {
        $mdThemingProvider.theme('indigo')
            .primaryPalette('indigo')
            .accentPalette('pink');

        $mdThemingProvider.theme('lime')
            .primaryPalette('lime')
            .accentPalette('orange')
            .warnPalette('blue');
        $mdThemingProvider.alwaysWatchTheme(true);
    }).directive('compareTo', compareTo);

var compareTo = function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
};