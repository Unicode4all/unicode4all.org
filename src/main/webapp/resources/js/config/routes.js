unisite.config(function ($locationProvider, $routeProvider, $httpProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider

    // route for the home page
        .when('/', {
            templateUrl: 'resources/pages/blog.html',
            controller: 'blogController'
        })

    // route for the about page
        .when('/about', {
            templateUrl: 'resources/pages/about.html',
            controller: 'aboutController'
        })
        .when('/login', {
            templateUrl: 'resources/pages/login.html',
            controller: 'loginController'
        })
        .when('/register', {
            templateUrl: 'resources/pages/signup.html',
            controller: 'SignupController',
            controllerAs: 'signupcontroller'
        }).otherwise('/');

        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

});