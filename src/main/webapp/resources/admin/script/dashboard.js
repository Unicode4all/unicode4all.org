$(document).ready(function () {/* your code */
    $('#content').load("admin/index");
    $('.sidebar-collapse a').click(function() {
        var id = $(this).attr('id');
        switch(id){
            case 'index':
                loadPage("admin/index");
                break;
            case 'posts':
                loadPage("admin/posts");
                break;
            default:
                break;
        }
        $(this).parent().siblings('li').removeClass('active');
        $(this).parent().addClass('active');
    });

    function loadPage(page) {
        $('#content').load(page);
    }
});

